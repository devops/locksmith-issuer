# Locksmith Issuer

## External route

```yaml
kind: Route
apiVersion: route.openshift.io/v1
metadata:
  name: my-route
  labels:
    cloud.duke.edu/route-type: external
  annotations:
    cert-manager.io/issuer-kind: Issuer
    cert-manager.io/issuer-name: locksmith-issuer-external
spec:
```

## Internal (Duke-only) route

```yaml
kind: Route
apiVersion: route.openshift.io/v1
metadata:
  name: my-route
  annotations:
    cert-manager.io/issuer-kind: Issuer
    cert-manager.io/issuer-name: locksmith-issuer-internal
spec:
```
